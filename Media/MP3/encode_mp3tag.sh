#!/usr/bin/env bash

##################################################
##						##	
##		MP3 TAG ENCODER			##
##						##
##	Requary install this package 		##
##						##
##  mutagen libtag1-rusxmms libtag libtaginfo	##
##						##
##	     Write from vint_243		##
##						##
##################################################


opt=$1
path=$2
param=$3

 	if [[ $opt == "cp1251" ]] ; then

	      	echo
	      	echo "Auto encode MP3 Tag from CP1251"

	      	find $path -iname '*.mp3' -print0 | xargs -0 mid3iconv -e CP1251 --remove-v1
		
	      	echo 
	      	echo "Encode MP3 Tag done"
		echo

	fi
	
	if [[ $opt == "utf8" ]] ; then

                echo
                echo "Auto encode MP3 Tag from UTF-8"

                find $path -iname '*.mp3' -print0 | xargs -0 mid3iconv -e UTF-8 --remove-v1

                echo 
                echo "Encode MP3 Tag done"
                echo

        fi

	if [[ $opt == "manual" ]] ; then

                echo
                echo "Manual encode MP3 Tag from $param"

                find $path -iname '*.mp3' -print0 | xargs -0 mid3iconv -e $param --remove-v1

                echo 
                echo "Encode MP3 Tag done"
                echo

        fi

	if [[ $opt == "erase" ]] ; then
		
		echo 
		echo "Erase MP3 tag from this file"
		
		find $path -iname '*.mp3' -print0 | xargs -0 mid3v2 -v -d
		find $path -iname '*.mp3' -print0 | xargs -0 mid3v2 -v -s

		echo
		echo "Erase MP3 tag done"
		echo

	fi

	if [[ $opt == '' ]] ;then 
		
		opt='--help'

	fi
 
	if [[ $opt == "--help" ]] ; then

	 	echo
		echo "Help page from encode_mp3tag"
		echo
		echo "encode_mp3tag OPTIONS PATH SET"
		echo ""
		echo "              OPTIONS  Auto encode MP3 Tag from :"
		echo "                       cp1251 - Encode MP3 Tag from CP1251"
		echo "                       utf8   - Encode MP3 Tag from UTF-8"
		echo "                       manual - Set manual charset from MP3 Tag. Using "SET""
		echo "                           SET  Enter manual charset from MP3 Tag"
		echo 
		echo "                      PATH  Enter PATH from MP3 files"
		echo 
 		
	fi

	     

